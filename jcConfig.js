var constants, paths;

constants = {
  ASSETS_URL: '//dxnqhjr6ye2au.cloudfront.net'
};

paths = {
  assets: 'assets',
  build: 'build',
  bower: 'bower_components',
  css: 'css',
  data: 'data',
  fonts: 'fonts',
  ico: 'ico',
  js: 'js',
  layout: 'layout',
  tmp: 'tmp',
  partials: 'partials',
  tests: 'tests',
  vendor: 'vendor',
  wraith: 'wraith'
};

exports.constants = constants;
exports.paths = paths;
